import { IObject, ITreeNode } from "../../interfaces/i-object";

export function createTree(objects: IObject[]): ITreeNode[] {
  const objTree: ITreeNode[] = [];
  // for (let i = 0; i < 4; i++) {
  //   if (i === 0) {
  //     const rootObj = objects.find((item) => item.objType === 0);
  //     if (rootObj) {
  //       const pushedNode: ITreeNode = {
  //         ...rootObj,
  //         childrens: [],
  //       };
  //       objTree.push(pushedNode);
  //     }
  //   }
  //   if (i === 1) {
  //     for (const obj of objects) {
  //       if (obj.parentId === objTree[0].id) {
  //         const pushedNode: ITreeNode = {
  //           ...obj,
  //           childrens: [],
  //         };
  //         objTree[0].childrens.push(pushedNode);
  //       }
  //     }
  //   }
  //   if (i === 2) {
  //     for (const obj of objects) {
  //       for (const prod of objTree[0].childrens) {
  //         if (obj.parentId === prod.id) {
  //           const pushedNode: ITreeNode = {
  //             ...obj,
  //             childrens: [],
  //           };
  //           objTree[0].childrens.forEach((elem) => {
  //             if (elem.id === pushedNode.parentId) {
  //               elem.childrens.push(pushedNode);
  //             }
  //           });
  //         }
  //       }
  //     }
  //   }
  //   if (i === 3) {
  //     for (const obj of objects) {
  //       for (const prod of objTree[0].childrens) {
  //         for (const plant of prod.childrens) {
  //           if (obj.parentId === plant.id) {
  //             const pushedNode: ITreeNode = {
  //               ...obj,
  //               childrens: [],
  //             };
  //             objTree[0].childrens.forEach((elem) => {
  //               elem.childrens.forEach((item) => {
  //                 if (item.id === pushedNode.parentId) {
  //                   item.childrens.push(pushedNode);
  //                 }
  //               });
  //             });
  //           }
  //         }
  //       }
  //     }
  //   }
  // }
  return objTree;
}
