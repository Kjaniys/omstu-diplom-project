import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IObject, ITreeNode } from "../../interfaces/i-object";

interface ObjectsManager {
    selectedObjects: IObject[] | null | undefined;
}

const INITIAL_STATE_OBJECTS_MANAGER: ObjectsManager = {
    selectedObjects: null,
}

const objectsManagerSlice = createSlice({
    name: "objectsManager",
    initialState: INITIAL_STATE_OBJECTS_MANAGER,
    reducers: {
        selectObjects: (
            state,
            action: PayloadAction<IObject[] | null | undefined>
        ) => {
            state.selectedObjects = action.payload;
        },
    }
});

export const { selectObjects } = objectsManagerSlice.actions;

export default objectsManagerSlice;