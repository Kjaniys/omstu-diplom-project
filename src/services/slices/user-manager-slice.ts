import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface UserManager {
    selectedFullName: string | null | undefined;
}

const INITIAL_STATE_USER_MANAGER: UserManager = {
    selectedFullName: null,
}

const userManagerSlice = createSlice({
    name: "userManager",
    initialState: INITIAL_STATE_USER_MANAGER,
    reducers: {
        selectFullName: (
            state,
            action: PayloadAction<string | null | undefined>
        ) => {
            state.selectedFullName = action.payload;
        },
    }
});

export const { selectFullName } = userManagerSlice.actions;

export default userManagerSlice;