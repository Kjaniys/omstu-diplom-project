import { configureStore } from "@reduxjs/toolkit";
import { Api } from "./API/api-redux";
import userManagerSlice from "./slices/user-manager-slice";

export const store = configureStore({
    reducer: {
        [Api.reducerPath]: Api.reducer,
        [userManagerSlice.name]: userManagerSlice.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }).concat(Api.middleware),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
