import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IObject } from "../../interfaces/i-object";

export const Api = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://25.45.185.122:7017/",
    credentials: "include",
    mode: "no-cors",
  }),
  tagTypes: ["Tags"],
  endpoints: (builder) => ({
    getObjects: builder.query<IObject, any>({
      query: () => ({
        url: `FabricObject`,
      }),
    }),
  }),
});

export const { useLazyGetObjectsQuery } = Api;
