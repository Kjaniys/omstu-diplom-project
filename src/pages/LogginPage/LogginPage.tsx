import './LogginPage.scss';
import React, { useState } from 'react';
import { Text } from '@consta/uikit/Text';
import { TextField } from '@consta/uikit/TextField';
import { Button } from '@consta/uikit/Button';
import { Layout } from '@consta/uikit/Layout';
import { useDispatch } from 'react-redux';
import { selectFullName } from '../../services/slices/user-manager-slice';
import { useNavigate } from 'react-router-dom';
import { validStatuses } from '../../interfaces/i-status';

const LogginPage: React.FC = () => {
    const [name, setName] = useState<string | null>(null);
    const handleChangeName = ({ value }: { value: string | null }) => setName(value);
    const [surname, setSurame] = useState<string | null>(null);
    const handleChangeSurname = ({ value }: { value: string | null }) => setSurame(value);
    const [validStatus, setValidStatus] = useState<validStatuses>();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    function setFullName(name: string | null, surname: string | null) {
        if (name && surname) {
            dispatch(selectFullName((`${surname} ${name}`).trim()));
            setValidStatus(validStatuses.Success);
            navigate("/hub");
            return;
        }
        setValidStatus(validStatuses.Alert);
    }

    return (
        <div className="loggin-page">
            <Layout className="loggin-layout" direction='column'>
                <Text className="loggin-page-title" size="6xl">Вход</Text>
                <Layout className="loggin-page-form" direction='column'>
                    <TextField
                        placeholder="Имя"
                        value={name}
                        size="l"
                        status={validStatus}
                        onChange={handleChangeName}
                    />
                    <TextField
                        placeholder="Фамилия"
                        value={surname}
                        size="l"
                        status={validStatus}
                        onChange={handleChangeSurname}
                    />
                    <Button label="Подтвердить" onClick={() => setFullName(name, surname)} />
                </Layout>
            </Layout>
        </div>
    )
}

export default LogginPage;
