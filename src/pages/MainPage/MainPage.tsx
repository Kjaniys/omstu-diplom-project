import { useDispatch, useSelector } from "react-redux";
import "./MainPage.scss";
import React, { useEffect, useState } from "react";
import { RootState } from "../../services/store";
import HeaderPart from "../../parts/HeaderPart/HeaderPart";
import { Layout } from "@consta/uikit/Layout";
import { IObject, ITreeNode } from "../../interfaces/i-object";
//import { createTree } from "../../services/utils/tree-creator";
import { selectObjects } from "../../services/slices/objects-manager-slice";
import ObjectSelectorPart from "../../parts/ObjectSelectorPart/ObjectSelectorPart";
import { useLazyGetObjectsQuery } from "../../services/API/api-redux";

const MainPage: React.FC = () => {
  const [objects, setObjects] = useState<IObject[]>([]);
  const [getObjects] = useLazyGetObjectsQuery()

  const dispatch = useDispatch();
  useEffect(() => {
    const objects: IObject[] = require("../../mocks/objMock.json");
    setObjects(objects);
    dispatch(selectObjects(objects));
    getObjects([]).then(result => {
      console.log("OBJECTS", result);
    })
  }, []);

  return (
    <div className="main-page">
      <Layout className="main-page-header">
        <HeaderPart />
      </Layout>
      <Layout className="main-page-container">
        <Layout className="main-page-left">
          <ObjectSelectorPart objects={objects} />
        </Layout>
        <Layout className="main-page-right"></Layout>
      </Layout>
    </div>
  );
};

export default MainPage;
