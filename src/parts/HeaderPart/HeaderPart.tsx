import './HeaderPart.scss';
import React, { useState } from 'react';
import { Text } from '@consta/uikit/Text';
import { Button } from '@consta/uikit/Button';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../services/store';
import { useNavigate } from 'react-router-dom';
import { Layout } from '@consta/uikit/Layout';
import { selectFullName } from '../../services/slices/user-manager-slice';
import { Select } from '@consta/uikit/Select';
import { Item } from '../../interfaces/i-item';

const items: Item[] = [
    {
        label: 'Просмотр тегов',
        id: 1,
    },
    {
        label: 'Мониторинг тегов',
        id: 2,
    },
];

const HeaderPart: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [pageValue, setPageValue] = useState<Item | null>(items[0]);
    const selectedFullName = useSelector(
        (state: RootState) => state.userManager.selectedFullName
    )

    function resetUser() {
        dispatch(selectFullName(null));
        navigate("/loggin")
    }

    return (
        <div className="header-part">
            <Layout className="header-layout">
                <Layout className="header-left">
                    <Text className="header-elem" size="3xl">Система</Text>
                    <Select items={items} value={pageValue} onChange={({ value }) => setPageValue(value)} />
                </Layout>
                <Layout className="header-right">
                    <Text size="xl">{selectedFullName}</Text>
                    <Button className="header-elem" label="Выйти" onClick={() => resetUser()} />
                </Layout>
            </Layout>
        </div>
    )
}

export default HeaderPart;