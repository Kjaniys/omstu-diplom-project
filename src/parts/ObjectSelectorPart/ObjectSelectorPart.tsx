import "./ObjectSelectorPart.scss";
import React, { Key, ReactNode, useEffect, useState } from "react";
import { Text } from "@consta/uikit/Text";
import { Button } from "@consta/uikit/Button";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../services/store";
import { useNavigate } from "react-router-dom";
import { Layout } from "@consta/uikit/Layout";
import { selectFullName } from "../../services/slices/user-manager-slice";
import { Select } from "@consta/uikit/Select";
import { Item } from "../../interfaces/i-item";
import { IObject, ITreeNode } from "../../interfaces/i-object";
import { Collapse } from "@consta/uikit/Collapse";
import { rcTreeAdapter } from "@consta/rc-tree-adapter/rcTreeAdapter";
import { default as RCTree } from "rc-tree";

const ObjectSelectorPart: React.FC<{ objects: IObject[] }> = ({ objects }) => {
  const [objectsForTree, setObjectsForTree] = useState<ITreeNode[]>([]);
  const [tree, setTree] = useState<ITreeNode[]>([]);
  const [selectedObjectsKeys, setSelectedObjectsKeys] = useState<Key[]>([]);
  const [selectedObject, setSelectedObject] = useState<IObject>();

  useEffect(() => {
    if (objects) {
      const rowTreeData: ITreeNode[] = [];
      for (const obj of objects) {
        const pushedNode: ITreeNode = {
          ...obj,
          key: obj.id,
          title: obj.name,
          children: [],
        };
        rowTreeData.push(pushedNode);
      }
      rowTreeData.sort((a, b) => {
        const nameA = a.title.toLowerCase(),
          nameB = b.title.toLocaleLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });
      setObjectsForTree(rowTreeData);
    }
  }, [objects]);

  useEffect(() => {
    if (objectsForTree.length) {
      const finalTree: ITreeNode[] = createTree(objectsForTree);
      setTree(finalTree);
    }
  }, [objectsForTree]);

  function createTree(arrOfObjects: ITreeNode[]) {
    const tree = [],
      map = new Map();
    for (let i = 0; i < arrOfObjects.length; ++i) {
      const item = arrOfObjects[i];
      console.log("item", item);
      const mapItem = map.get(item.id);
      if (!mapItem || Array.isArray(mapItem)) {
        if (mapItem) {
          item.children = mapItem;
        }
        map.set(item.id, item);
      }
      if (item.parentId === "") {
        tree.push(item);
      } else {
        const parentItem = map.get(item.parentId);
        if (!parentItem) {
          map.set(item.parentId, [item]);
        } else {
          const children = Array.isArray(parentItem)
            ? parentItem
            : (parentItem.children = parentItem.children || []);
          children.push(item);
        }
      }
    }
    return tree;
  }

  useEffect(() => {
    if (selectedObject) {
      setSelectedObjectsKeys([selectedObject.id]);
    } else {
      setSelectedObjectsKeys([tree[0]?.id || ""]);
    }
  }, [selectedObject, tree]);

  useEffect(() => {
    if (!selectedObjectsKeys.length || !objects.length) return;
    const curObjId = selectedObjectsKeys.at(0);
    if (selectedObject?.id === curObjId) return;
    const findedObject = objects.find((item) => item.id === curObjId);
    if (findedObject) {
      setSelectedObject(findedObject);
    }
  }, [selectedObjectsKeys, objects]);

  function genTree(objs: ITreeNode[]): ReactNode {
    const treeProps = rcTreeAdapter({ size: "m" });
    return (
      <RCTree
        {...treeProps}
        defaultExpandAll
        treeData={objs}
        onSelect={(selectedNode) => {
          setSelectedObjectsKeys(selectedNode);
        }}
        selectedKeys={selectedObjectsKeys}
      />
    );
  }


  return (
    <div className="objects-part">
      <Layout className="objects-part-container" direction="column">
        <Layout className="objects-part-header">
          <Text view="primary" size="2xl">
            Объекты
          </Text>
        </Layout>
        <Layout className="objects-tree">
          {objects.length && genTree(tree)}
        </Layout>
      </Layout>
    </div>
  );
};

export default ObjectSelectorPart;
