const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://25.45.185.122:7017',
      changeOrigin: true,
    })
  );
};