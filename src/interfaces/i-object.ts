export interface IObject {
    id: string; // id объекта
    parentId: string; // id родителя объекта
    name: string; // название объекта
    objType: number; // тип объекта: 0 - завод, 1 - производство, 2 - установка, 3 - оборудование
}

export interface ITreeNode extends IObject {
    key: string;
    title: string;
    children: ITreeNode[];
}