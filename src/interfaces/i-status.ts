export enum validStatuses {
    Alert = "alert",
    Success = "success",
    Warning = "warning",
}