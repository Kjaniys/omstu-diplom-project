export interface ITag {
  name: string; //Русское название тега
  tag: string; //Оригинальное название тега (для поиска и т.д.)
  description: string; //Описание тега на русском (о чем тег)
  unit: string; //единицы измерения тега
  parentId: string; //id оборудования по тегу
  status: tagStatus; //статус тега
  points: IPoint[]; //массив точек
}

export interface IPoint {
  d: string; //дата
  v: string; //значение
}

export enum tagStatus {
    Bad = "Bad",
    Good = "Good",
}