import './App.scss';
import React, { useEffect } from 'react';
import LogginPage from './pages/LogginPage/LogginPage';
import { useSelector } from 'react-redux';
import { RootState } from './services/store';
import { HashRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Theme, presetGpnDark } from '@consta/uikit/Theme';
import MainPage from './pages/MainPage/MainPage';

const App: React.FC = () => {
  const selectedFullName = useSelector(
    (state: RootState) => state.userManager.selectedFullName
  )
  useEffect(() => {
    if (selectedFullName) {
    }
  }, [selectedFullName])

  function genLogginPage(): React.ReactNode {
      return <Route path="/loggin" element={<LogginPage />} />
  }

  function genStartingPage(): React.ReactNode {
    return <Route path="*" element={<Navigate to="/loggin"/>} />
}

  function genMainPage(user: string | null | undefined): React.ReactNode {
    if (user) {
      return <Route path="/hub" element={<MainPage />} />
    }
  }

  return (
    <Theme className="theme" preset={presetGpnDark}>
      <div className="body">
        <HashRouter>
          <Routes>
            {genLogginPage()}
            {genMainPage(selectedFullName)}
            {genStartingPage()}
          </Routes>
        </HashRouter>
      </div>
    </Theme>
  )
}

export default App;
